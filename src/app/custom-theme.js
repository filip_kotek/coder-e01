import { createTheme } from "@material-ui/core";

const customTheme = createTheme({
  palette: {
    primary: {
      main: "#3bd192",
    },
    secondary: {
      main: "#9e9f9e",
    },
  },
});

export default customTheme;
