import { configureStore } from '@reduxjs/toolkit';
import rootReducer from './redux/root/rootSlice';

export const store = configureStore({
  reducer: {
    root: rootReducer,
  },
});
