import {
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Container,
  Grid,
  makeStyles,
  Slide,
  Typography,
} from "@material-ui/core";
import React from "react";
import { useInView } from "react-intersection-observer";
import FullPageCenteredWrapper from "./../FullPageCenteredWrapper";
import mockData from "./../../mock-data";

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: 200,
    maxWidth: 300,
    margin: theme.spacing(2),
  },
  media: {
    height: 200,
  },
}));

const MainSection = () => {
  const classes = useStyles();
  const { ref, inView } = useInView({ threshold: 0.3 });

  return (
    <FullPageCenteredWrapper ref={ref}>
      <Container>
        <Typography paragraph>
          Main section. Put here whatever according to assignment.
        </Typography>
        <Typography paragraph color={"primary"}>
          This is primary color chosen by our "client". Incorporate it well.
        </Typography>
        <Typography paragraph color={"secondary"}>
          This is secondary color chosen by our "client". Incorporate it well.
        </Typography>
        <Typography paragraph>
          If you don't like those colors, choose your own schema wisely:-)
        </Typography>
        <Grid container>
          {mockData.map(({ procedure, description, duration }, index) => (
            <Slide
              in={inView}
              direction={index % 2 ? "right" : "left"}
              timeout={1000}
              key={procedure}
            >
              <Grid item xs={12} sm={6}>
                <Card className={classes.root}>
                  <CardActionArea>
                    <CardMedia
                      className={classes.media}
                      image="https://picsum.photos/200"
                      title={procedure}
                    />
                    <CardContent>
                      <Typography gutterBottom variant="h5" component="h2">
                        {`${procedure} (${duration}min)`}
                      </Typography>
                      <Typography
                        variant="body2"
                        color="textSecondary"
                        component="p"
                      >
                        {description}
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
            </Slide>
          ))}
        </Grid>
      </Container>
    </FullPageCenteredWrapper>
  );
};

export default MainSection;
